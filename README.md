
# Hi, this repo has been made public in time for #pyconau 2019, probably a bit too early to be really useful.

# Videogrammetry Resources

Resources for Volumetric Capture and Playback. 
Sometimes called Videogrammetry or Volumetric Video. 
This is a collection of software, readings, tools and designs for helping people get into the scene.
It has an emphasis on open source.

If you have a suggestion please open a discussion on the [gitlab page for the project](https://gitlab.com/dodgyville/videogrammetry). 


Volumetric Capture is a three step process:
1. Capture footage with a rig
2. Process raw footage into a volumetric video file
3. Playback the file

There are a lot of different techniques to use at each step of the way, here is an overview of some of the different methods and tools you can use.


# Camera Rigs

### Software
* [voltfnode](https://gitlab.com/dodgyville) Android App and Web server for controlling
camera rigs made of android phones.
* [fiducialary](https://gitlab.com/dodgyville/fiducialary) An open source app for generating unique circular tracking markers

### Hardware

#### RGB cameras
Rigs can be made with android, usb or DSLR cameras.   

#### Depth cameras
Rigs can also be made with cameras which measure depth such as RGBD or lidar but the best results at the moment come from RGB cameras 
* sensor
* structure
* kinnect pro


### Tutorials
* [building a rig](https://gitlab.com/dodgyville)
* [taking a good set of photos for photogrammetry]


### Readings
* Know of a good paper or article on the topic? Open a discussion on the [gitlab page for the project](https://gitlab.com/dodgyville/videogrammetry).
* [3D Reconstruction with Stereo Images — Part 3: Epipolar Geometry - Keenan James](https://medium.com/@dc.aihub/3d-reconstruction-with-stereo-images-part-3-epipolar-geometry-98b75e40f59d)


# Processing 

### Software
* [agisoft metashape](https://www.agisoft.com/) ($) A closed-source photogrammetry tool that can be used to generate volumetric frames
* [pypsxlib](https://gitlab.com/dodgyville/pypsxlib) Library for handling metashape projects
* [voltfnode](https://gitlab.com/dodgyville/voltfnode) An application built on flask that can manage compositing footage into volumetric video. 

### Readings
* Know of a good paper or article on the topic? Open a discussion on the [gitlab page for the project](https://gitlab.com/dodgyville/videogrammetry).


# Playback

### Software
* [voltfplayer](https://gitlab.com/dodgyville/) An volumetric video player app built in godot. (not public yet)
* [godot](https://godotengine.org/) An open-source 3D game engine that can be used in volumetric video

### Readings
* Know of a good paper or article on the topic? Open a discussion on the [gitlab page for the project](https://gitlab.com/dodgyville/videogrammetry).


## Dedicated studios
There are some dedicated vol capture studios out there, let us know by opening a discussioning on the [gitlab page for the project](https://gitlab.com/dodgyville/videogrammetry).
if you're a film maker these can be hired.


## Other


### Changelog
* Initial release

### Contributors
* Luke Miller

### Thanks
* Film Victoria
* Creative Victoria

